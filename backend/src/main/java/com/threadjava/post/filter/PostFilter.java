package com.threadjava.post.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostFilter {

    private UUID userId;

    private Boolean isDeleted;
    /**
     * If null - all posts are shown;
     * If true - shown posts that belong to this user;
     * If false - shown posts that does not belong to this user;
     * Filter is ignored, if userId is null
     */
    private Boolean showUserAndNotOthers;
    /**
     * If null - all posts are shown;
     * If true - shown posts that this user liked;
     * If false - shown posts that this user disliked;
     * Filter is ignored, if userId is null
     */
    private Boolean showPostsThatWereLiked;

}
