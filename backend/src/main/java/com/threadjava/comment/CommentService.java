package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final UsersService usersService;

    public CommentService(CommentRepository commentRepository, UsersService usersService) {
        this.commentRepository = commentRepository;
        this.usersService = usersService;
    }

    public void softDeleteComment(UUID id) {
        commentRepository.softDeleteById(id);
    }

    public CommentDetailsDto getCommentById(UUID id) {
        CommentDetailsDto commentDetailsDto = commentRepository.getCommentDetailsDtoById(id)
                .map(CommentMapper.MAPPER::commentDetailsQueryResultToDetails)
                .orElseThrow();
        List<UserDetailsDto> allByReactionOnComment = usersService.getAllByReactionOnComment(id, true);
        commentDetailsDto.setUsersWhoLikedComment(allByReactionOnComment);
        return commentDetailsDto;
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        Comment comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        Comment postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public void update(UUID id, CommentUpdateDto commentUpdateDto) {
        Comment comment = CommentMapper.MAPPER.commentUpdateDtoToModel(commentUpdateDto);
        commentRepository.update(id, comment);
    }
}
