package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    @Query("UPDATE Comment c SET c.isDeleted = true WHERE c.id = :id")
    @Modifying
    @Transactional
    void softDeleteById(@Param("id") UUID id);

    @Query("UPDATE Comment c SET " +
            "c.body = :#{#updated.body} " +
            "WHERE c.id = :id AND c.user.id = :#{#updated.user.id}")
    @Modifying
    @Transactional
    void update(@Param("id") UUID id, @Param("updated") Comment updated);

    @Query("SELECT NEW com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, c.user, c.post.id, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt) " +
            "FROM Comment c " +
            "WHERE c.id = :commentId "
    )
    Optional<CommentDetailsQueryResult> getCommentDetailsDtoById(@Param("commentId") UUID commentId);

    @Query("SELECT NEW com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, c.user, c.post.id, " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
            "c.createdAt, c.updatedAt) " +
            "FROM Comment c " +
            "WHERE (( cast(:postId as string) is null OR c.post.id = :postId) AND c.isDeleted = :isDeleted) " +
            "order by c.createdAt desc")
    List<CommentDetailsQueryResult> findAllByPostIdAndIsDeleted(UUID postId, Boolean isDeleted);

}