package com.threadjava.comment.validation;

import com.threadjava.comment.dto.CommentUpdateDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class CommentUpdateDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, CommentUpdateDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        CommentUpdateDto dto = (CommentUpdateDto) target;
        validateBody(dto, errors);
    }

    private void validateBody(CommentUpdateDto dto, Errors errors) {
        if (dto.getBody() == null || dto.getBody().isBlank()) {
            errors.reject("comment.validation.body", "Body is required");
        }
    }
}
