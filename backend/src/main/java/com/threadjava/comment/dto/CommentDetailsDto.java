package com.threadjava.comment.dto;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import lombok.Data;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class CommentDetailsDto {
    private UUID id;
    private String body;
    private UserShortDto user;
    private UUID postId;
    private long likeCount;
    private long dislikeCount;
    private List<UserDetailsDto> usersWhoLikedComment = new ArrayList<>();
    private List<UserDetailsDto> usersWhoDislikedComment = new ArrayList<>();
}
