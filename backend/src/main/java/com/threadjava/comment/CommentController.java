package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.comment.validation.CommentUpdateDtoValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentUpdateDtoValidator updateDtoValidator;

    public CommentController(CommentService commentService, CommentUpdateDtoValidator updateDtoValidator) {
        this.commentService = commentService;
        this.updateDtoValidator = updateDtoValidator;
    }

    @InitBinder("commentUpdateDto")
    public void updateInitBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(updateDtoValidator);
    }

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.create(commentDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") UUID id) {
        commentService.softDeleteComment(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CommentUpdateDto> update(
            @PathVariable("id") UUID id,
            @Validated @RequestBody CommentUpdateDto commentUpdateDto,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        commentUpdateDto.setUserId(getUserId());
        commentService.update(id, commentUpdateDto);
        return new ResponseEntity<>(commentUpdateDto, HttpStatus.OK);
    }
}
