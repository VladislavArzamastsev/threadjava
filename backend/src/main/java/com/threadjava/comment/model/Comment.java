package com.threadjava.comment.model;

import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.db.BaseEntity;
import com.threadjava.post.model.Post;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "comments")
public class Comment extends BaseEntity {
    @Column(name = "body", columnDefinition = "TEXT")
    private String body;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "post_id")
    private Post post;

    @OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<CommentReaction> reactions = new ArrayList<>();

    @Transient
    private List<UserDetailsDto> usersWhoLikedComment = new ArrayList<>();

    @Transient
    private List<UserDetailsDto> usersWhoDislikedComment = new ArrayList<>();
}
