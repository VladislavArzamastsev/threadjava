package com.threadjava.users;

import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<User, UUID> {
    Optional<User> findByEmail(String email);

    @Query("UPDATE User u SET " +
            "u.username = :#{#updated.username}, " +
            "u.status = :#{#updated.status}, " +
            "u.email = :#{#updated.email}, " +
            "u.avatar = :#{#updated.avatar} " +
            "WHERE u.id = :#{#updated.id} ")
    @Transactional
    @Modifying
    void updateProfile(@Param("updated") User updated);

    @Query("UPDATE User u SET " +
            "u.password = :password " +
            "WHERE u.email = :email ")
    @Transactional
    @Modifying
    void updatePassword(@Param("email") String email, @Param("password") String password);

    @Query("SELECT u FROM User u INNER JOIN PostReaction pr " +
            "        ON pr.post.id = :postId AND u.id = pr.user.id AND pr.isLike = :liked")
    List<User> getAllByReactionOnPost(@Param("postId") UUID posId, @Param("liked") boolean liked);

    @Query("SELECT u FROM User u INNER JOIN CommentReaction cr " +
            "        ON cr.comment.id = :commentId AND u.id = cr.user.id AND cr.isLike = :liked")
    List<User> getAllByReactionOnComment(@Param("commentId") UUID commentId, @Param("liked") boolean liked);
}