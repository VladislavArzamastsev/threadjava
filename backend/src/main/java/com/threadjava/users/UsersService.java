package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public List<UserDetailsDto> getAllByReactionOnPost(UUID postId, boolean liked){
        return usersRepository
                .getAllByReactionOnPost(postId, liked)
                .stream()
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .collect(Collectors.toList());
    }

    public List<UserDetailsDto> getAllByReactionOnComment(UUID commentId, boolean liked){
        return usersRepository
                .getAllByReactionOnComment(commentId, liked)
                .stream()
                .map(UserMapper.MAPPER::userToUserDetailsDto)
                .collect(Collectors.toList());
    }

    public void updateUserProfile(UserDetailsDto dto){
        User user = UserMapper.MAPPER.userDetailsToUser(dto);
        usersRepository.updateProfile(user);
    }

    public void save(User user) {
        usersRepository.save(user);
    }

    public boolean userExists(String email){
        return usersRepository.findByEmail(email).isPresent();
    }

    public void updatePassword(String email, String password){
        usersRepository.updatePassword(email, password);
    }
}