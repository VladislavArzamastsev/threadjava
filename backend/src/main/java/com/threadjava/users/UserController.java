package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.validation.UserDetailsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UsersService userDetailsService;
    private final UserDetailsValidator userDetailsValidator;

    @Autowired
    public UserController(UsersService userDetailsService, UserDetailsValidator userDetailsValidator) {
        this.userDetailsService = userDetailsService;
        this.userDetailsValidator = userDetailsValidator;
    }

    @InitBinder("userDetailsDto")
    private void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(userDetailsValidator);
    }

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PutMapping
    public ResponseEntity<UserDetailsDto> updateUserProfile(
            @Validated @RequestBody UserDetailsDto userDetailsDto,
            BindingResult bindingResult) {
        if (! bindingResult.hasErrors()) {
            userDetailsService.updateUserProfile(userDetailsDto);
            return new ResponseEntity<>(userDetailsDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
}
