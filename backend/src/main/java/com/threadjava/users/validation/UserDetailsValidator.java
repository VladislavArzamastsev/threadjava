package com.threadjava.users.validation;

import com.threadjava.image.validation.ImageDtoValidator;
import com.threadjava.users.dto.UserDetailsDto;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class UserDetailsValidator implements Validator {

    private final ImageDtoValidator imageDtoValidator;

    public UserDetailsValidator(ImageDtoValidator imageDtoValidator) {
        this.imageDtoValidator = imageDtoValidator;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, UserDetailsDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDetailsDto dto = (UserDetailsDto) target;
        checkId(dto, errors);
        checkEmail(dto, errors);
        checkUsername(dto, errors);
        checkImage(dto, errors);
    }

    private void checkId(UserDetailsDto dto, Errors errors){
        if(dto.getId() == null){
            errors.reject("user.details.validation.id", "Id is required");
        }
    }

    private void checkEmail(UserDetailsDto dto, Errors errors){
        if(dto.getEmail() == null){
            errors.reject("user.details.validation.email.not-present",
                    "Email is required");
            return;
        }
        EmailValidator validator = EmailValidator.getInstance();
        if(!validator.isValid(dto.getEmail())){
            errors.reject("user.details.validation.email.invalid",
                    "Email is invalid");
        }
    }

    private void checkUsername(UserDetailsDto dto, Errors errors){
        if(dto.getUsername() == null){
            errors.reject("user.details.validation.username.not-present",
                    "Username is required");
        }else if(dto.getUsername().isEmpty()){
            errors.reject("user.details.validation.username.invalid",
                    "Username is invalid");
        }
    }

    private void checkImage(UserDetailsDto dto, Errors errors){
        if(dto.getImage() == null){
            return;
        }
        try {
            errors.pushNestedPath("image");
            ValidationUtils.invokeValidator(this.imageDtoValidator, dto.getImage(), errors);
        } finally {
            errors.popNestedPath();
        }
    }
}
