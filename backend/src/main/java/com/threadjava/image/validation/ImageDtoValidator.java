package com.threadjava.image.validation;

import com.threadjava.image.dto.ImageDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class ImageDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, ImageDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ImageDto dto = (ImageDto) target;
        checkId(dto, errors);
        checkLink(dto, errors);
    }

    private void checkId(ImageDto dto, Errors errors) {
        if (dto.getId() == null) {
            errors.reject("image.details.validation.id", "Id is required");
        }
    }

    private void checkLink(ImageDto dto, Errors errors) {
        if (dto.getLink() == null || dto.getLink().isBlank()) {
            errors.reject("image.details.validation.link",
                    "Link is required");
        }
    }
}
