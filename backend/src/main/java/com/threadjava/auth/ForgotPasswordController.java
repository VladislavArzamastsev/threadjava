package com.threadjava.auth;

import com.threadjava.auth.dto.ForgotPasswordDto;
import com.threadjava.auth.model.ChangePasswordDto;
import com.threadjava.auth.model.ForgotPasswordUser;
import com.threadjava.auth.validation.ChangePasswordDtoValidator;
import com.threadjava.auth.validation.ForgotPasswordDtoValidator;
import com.threadjava.notification.EmailTextNotificator;
import com.threadjava.users.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/auth/forgot-password")
public class ForgotPasswordController {

    private static final String TOKEN = "token";
    private static final String FORGOT_PASSWORD_SUBJECT = "Restore your Thread password";
    private static final String FORGOT_PASSWORD_MESSAGE_TEMPLATE = "If you forgot your password, click here:%n%s";

    private final AuthService authService;
    private final UsersService userDetailsService;
    private final ForgotPasswordDtoValidator forgotPasswordDtoValidator;
    private final ChangePasswordDtoValidator changePasswordDtoValidator;
    private final EmailTextNotificator emailTextNotificator;

    public ForgotPasswordController(AuthService authService, UsersService userDetailsService,
                                    ForgotPasswordDtoValidator forgotPasswordDtoValidator,
                                    ChangePasswordDtoValidator changePasswordDtoValidator,
                                    EmailTextNotificator emailTextNotificator) {
        this.authService = authService;
        this.userDetailsService = userDetailsService;
        this.forgotPasswordDtoValidator = forgotPasswordDtoValidator;
        this.changePasswordDtoValidator = changePasswordDtoValidator;
        this.emailTextNotificator = emailTextNotificator;
    }

    @InitBinder("forgotPasswordDto")
    public void forgotPasswordDtoBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(forgotPasswordDtoValidator);
    }

    @InitBinder("changePasswordDto")
    public void changePasswordDtoBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(changePasswordDtoValidator);
    }

    @PutMapping
    public void changePassword(
            @RequestParam(name = TOKEN) UUID token,
            @Validated @RequestBody ChangePasswordDto changePasswordDto,
            BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return;
        }
        Optional<ForgotPasswordUser> existing = authService.findForgotUserByToken(token);
        existing.ifPresent(forgotPasswordUser -> {
            authService.updatePassword(forgotPasswordUser.getEmail(), changePasswordDto.getPassword());
            authService.deleteForgotUserByEmail(forgotPasswordUser.getEmail());
        });
    }

    @GetMapping
    public Boolean verifyToken(@RequestParam(name = TOKEN) UUID token) {
        Optional<ForgotPasswordUser> existing = authService.findForgotUserByToken(token);
        return existing.map(forgotPasswordUser ->
                forgotPasswordUser.getExpiresAt().isAfter(LocalDateTime.now()))
                .orElse(Boolean.FALSE);
    }

    @PostMapping
    public ResponseEntity<Void> applyToForgotPassword(
            @Validated @RequestBody ForgotPasswordDto forgotPasswordDto,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (userDetailsService.userExists(forgotPasswordDto.getEmail())) {
            ForgotPasswordUser user = authService
                    .registerUserWhoForgotPassword(forgotPasswordDto.getEmail());
            String link = getLinkToPasswordChangePage(forgotPasswordDto.getLinkToPasswordChangePage(),
                    user.getToken());
            String message = String.format(FORGOT_PASSWORD_MESSAGE_TEMPLATE, link);
            emailTextNotificator.sendEmail(user.getEmail(), FORGOT_PASSWORD_SUBJECT, message);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private String getLinkToPasswordChangePage(String link, UUID token) {
        return String.format("%s?%s=%s", link, TOKEN, token.toString());
    }

}
