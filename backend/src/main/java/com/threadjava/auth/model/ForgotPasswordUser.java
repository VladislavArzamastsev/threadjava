package com.threadjava.auth.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "forgot_password")
public class ForgotPasswordUser {

    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "token", nullable = false, unique = true)
    private UUID token = UUID.randomUUID();

    @Column(name = "`timestamp`")
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "expires_at")
    private LocalDateTime expiresAt = LocalDateTime.now().plusHours(1);

}
