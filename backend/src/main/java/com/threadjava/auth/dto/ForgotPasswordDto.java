package com.threadjava.auth.dto;

import lombok.Data;

@Data
public class ForgotPasswordDto {

    private String email;
    private String linkToPasswordChangePage;

}
