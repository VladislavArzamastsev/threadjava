package com.threadjava.auth.validation;

import com.threadjava.auth.dto.ForgotPasswordDto;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class ForgotPasswordDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, ForgotPasswordDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ForgotPasswordDto dto = (ForgotPasswordDto) target;
        validateEmail(dto, errors);
        validateLink(dto, errors);
    }

    private void validateEmail(ForgotPasswordDto dto, Errors errors) {
        if (dto.getEmail() == null) {
            errors.reject("forgot-password.validation.email.not-present",
                    "Email is required");
        } else if (!EmailValidator.getInstance().isValid(dto.getEmail())) {
            errors.reject("forgot-password.validation.email.invalid",
                    "Email is invalid");
        }
    }

    private void validateLink(ForgotPasswordDto dto, Errors errors) {
        if (dto.getLinkToPasswordChangePage() == null) {
            errors.reject("forgot-password.validation.link.not-present",
                    "Link is required");
        }
    }
}
