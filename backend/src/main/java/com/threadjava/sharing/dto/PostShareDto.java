package com.threadjava.sharing.dto;

import lombok.Data;

@Data
public class PostShareDto {

    private String toEmail;
    private String link;

}
