package com.threadjava.postReactions;

import com.threadjava.notification.EmailTextNotificator;
import com.threadjava.post.PostsService;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.model.User;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {

    private final PostReactionService postsReactionsService;
    private final PostsService postsService;
    private final EmailTextNotificator emailTextNotificator;
    private final SimpMessagingTemplate template;

    public PostReactionController(PostReactionService postsReactionsService,
                                  PostsService postsService,
                                  EmailTextNotificator emailTextNotificator,
                                  SimpMessagingTemplate template) {
        this.postsReactionsService = postsReactionsService;
        this.postsService = postsService;
        this.emailTextNotificator = emailTextNotificator;
        this.template = template;
    }

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto dto) {
        dto.setUserId(getUserId());
        var reaction = postsReactionsService.setReaction(dto);

        if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {
            // notify a user if someone (not himself) liked his post
            template.convertAndSend("/topic/like", "Your post was liked!");
            performEmailNotification(dto);
        }
        return reaction;
    }

    private void performEmailNotification(ReceivedPostReactionDto dto) {
        Post post = postsService.getOne(dto.getPostId());
        User author = post.getUser();
        try {
            emailTextNotificator.sendEmail(author.getEmail(),
                    "Your post was liked!",
                    String.format("This post was liked: %n%s", post.getBody()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
