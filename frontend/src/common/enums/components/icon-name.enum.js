const IconName = {
  USER_CIRCLE: 'user circle',
  USERS_WHO_LIKED: 'smile',
  USERS_WHO_DISLIKED: 'frown',
  LOG_OUT: 'log out',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  CANCEL: 'cancel',
  COMMENT: 'comment',
  SHARE_ALTERNATE: 'share alternate',
  DELETE: 'trash',
  EDIT: 'edit',
  FROWN: 'frown',
  IMAGE: 'image',
  SUBMIT: 'check',
  COPY: 'copy'
};

export { IconName };
