import { createAction } from '@reduxjs/toolkit';
import { StorageKey } from 'src/common/enums/enums';
import { auth as authService, storage as storageService, userService } from 'src/services/services';

const ActionType = {
  SET_USER: 'profile/set-user'
};

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user
  }
}));

const login = request => async dispatch => {
  const { user, token } = await authService.login(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const applyForForgotPassword = (email, linkToPasswordChangePage) => async () => {
  await authService.applyForForgotPassword(email, linkToPasswordChangePage);
};

const verifyForgotPasswordToken = search => {
  const urlParams = new URLSearchParams(search);
  const token = urlParams.get('token');
  return authService.verifyForgotPasswordToken(token);
};

const changePassword = (search, password, repeatedPassword) => {
  const urlParams = new URLSearchParams(search);
  const token = urlParams.get('token');
  authService.changePassword(token, password, repeatedPassword);
};

const register = request => async dispatch => {
  const { user, token } = await authService.registration(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const logout = () => dispatch => {
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await authService.getCurrentUser();

  dispatch(setUser(user));
};

const updateUserProfile = user => async dispatch => {
  const updated = await userService.update(user);
  if (updated) {
    dispatch(setUser(user));
  }
};

export { setUser,
  login,
  applyForForgotPassword,
  changePassword,
  register,
  logout,
  loadCurrentUser,
  updateUserProfile,
  verifyForgotPasswordToken };
