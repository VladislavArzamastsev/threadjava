import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Post {
  constructor({ http }) {
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load('/api/posts', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getPost(id) {
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  addPost(payload) {
    return this._http.load('/api/posts', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  #addPostReaction(postId, isLikeValue) {
    return this._http.load('/api/postreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: isLikeValue
      })
    });
  }

  likePost(postId) {
    return this.#addPostReaction(postId, true);
  }

  dislikePost(postId) {
    return this.#addPostReaction(postId, false);
  }

  updatePost(id, updatedPost) {
    const imgId = updatedPost.image ? updatedPost.image.id : undefined;
    return this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        imageId: imgId,
        body: updatedPost.body
      })
    });
  }

  deletePost(id) {
    this._http.load(`/api/posts/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  sharePost(emailOfRecipient, linkToPost) {
    this._http.load('/api/share/post', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        toEmail: emailOfRecipient,
        link: linkToPost
      })
    });
  }
}

export { Post };
