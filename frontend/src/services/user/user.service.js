import { HttpMethod, ContentType } from 'src/common/enums/enums';

class UserService {
  constructor({ http }) {
    this._http = http;
  }

  update(user) {
    return this._http.load('/api/user', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(user)
    });
  }
}

export { UserService };
