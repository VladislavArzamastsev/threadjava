import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  login(payload) {
    return this._http.load('/api/auth/login', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load('/api/auth/register', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  verifyForgotPasswordToken(token) {
    return this._http.load(`/api/auth/forgot-password?token=${token}`, {
      method: HttpMethod.GET,
      hasAuth: false
    });
  }

  applyForForgotPassword(userEmail, passwordChangePageUrl) {
    this._http.load('/api/auth/forgot-password', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify({
        email: userEmail,
        linkToPasswordChangePage: passwordChangePageUrl
      })
    });
  }

  changePassword(token, newPassword, repeatedNewPassword) {
    this._http.load(`/api/auth/forgot-password?token=${token}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify({
        password: newPassword,
        repeatedPassword: repeatedNewPassword
      })
    });
  }

  getCurrentUser() {
    return this._http.load('/api/user', {
      method: HttpMethod.GET
    });
  }
}

export { Auth };
