import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Comment as CommentUI } from 'src/components/common/common';
import UserListEntry from './user-list-entry';

const UserList = ({ users, close }) => (
  <Modal
    dimmer="blurring"
    open
    onClose={close}
  >
    <Modal.Content>
      <CommentUI.Group style={{ maxWidth: '100%' }}>
        {users.map(user => (<UserListEntry user={user} />))}
      </CommentUI.Group>
    </Modal.Content>
  </Modal>
);

UserList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  users: PropTypes.array.isRequired,
  close: PropTypes.func.isRequired
};

export default UserList;
