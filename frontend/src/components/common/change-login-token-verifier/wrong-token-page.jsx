import * as React from 'react';
import { AppRoute } from 'src/common/enums/app/app-route.enum';
import { NavLink } from 'src/components/common/common';

const WrongTokenPage = () => (
  <div style={{ padding: '10px', textAlign: 'center', fontSize: 40 }}>
    <span>Ooops... looks like you have invalid token :(</span>
    <br />
    <br />
    <NavLink exact to={AppRoute.LOGIN}>
      Back to login
    </NavLink>
  </div>
);

export default WrongTokenPage;
