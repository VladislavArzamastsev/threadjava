import PropTypes from 'prop-types';
import { Form, Modal } from 'semantic-ui-react';
import * as React from 'react';
import { Button } from 'src/components/common/common';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import styles from './styles.module.scss';

const UpdateComment = ({ comment, onCommentUpdate, close }) => {
  const updatedComment = { ...comment };

  const [body, setBody] = React.useState(updatedComment.body);

  const handleUpdate = () => {
    if (!body || updatedComment.body === body) {
      return;
    }
    updatedComment.body = body;
    onCommentUpdate(comment.id, updatedComment);
  };

  const bodyChanged = data => {
    setBody(data === '' ? undefined : data);
  };

  return (
    <Modal open onClose={close} style={{ marginTop: '10%' }}>
      <Modal.Header className={styles.header}>
        <span>Edit Comment</span>
      </Modal.Header>
      <Modal.Content>
        <Form>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="Do you want to comment?"
            onChange={ev => bodyChanged(ev.target.value)}
          />
          <div style={{ float: 'left' }}>
            <Button
              color="teal"
              iconName={IconName.SUBMIT}
              onClick={handleUpdate}
            >
              <label className={styles.btnImgLabel}>
                Confirm
              </label>
            </Button>
          </div>
          <br />
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdateComment.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  comment: PropTypes.object.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdateComment;
