import * as React from 'react';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import { profileActionCreator } from 'src/store/actions';
import { AppRoute } from 'src/common/enums/enums';
import { Button, Form, Modal } from '../common/common';
import styles from './styles.module.scss';

const ChangePasswordForm = () => {
  const search = `${window.location.search}`;
  const [password, setPassword] = React.useState('');
  const [isPasswordValid, setIsPasswordValid] = React.useState(true);
  const [repeatedPassword, setRepeatedPassword] = React.useState('');
  const [isRepeatedPasswordValid, setIsRepeatedPasswordValid] = React.useState(true);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const repeatedPasswordChanged = data => {
    setRepeatedPassword(data);
    setIsRepeatedPasswordValid(true);
  };

  const checkRepeatedPassword = repeated => Boolean(repeated) && password === repeated;

  const handleChangePassword = () => {
    const isValid = isPasswordValid && isRepeatedPasswordValid && (password === repeatedPassword);
    if (!isValid) {
      return;
    }
    profileActionCreator.changePassword(search, password, repeatedPassword);
    window.location.replace(AppRoute.LOGIN);
  };

  return (
    <Modal open>
      <Modal.Header className={styles.header}>
        <span>Change password</span>
      </Modal.Header>
      <Modal.Content>
        <Form>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!isPasswordValid}
            onChange={ev => passwordChanged(ev.target.value)}
            onBlur={() => setIsPasswordValid(Boolean(password))}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Repeat password"
            type="password"
            error={!isRepeatedPasswordValid}
            onChange={ev => repeatedPasswordChanged(ev.target.value)}
            onBlur={() => setIsRepeatedPasswordValid(checkRepeatedPassword(repeatedPassword))}
          />
          <div>
            <div style={{ float: 'left' }}>
              <Button
                color="teal"
                iconName={IconName.SUBMIT}
                onClick={handleChangePassword}
              >
                <label className={styles.btnImgLabel}>
                  Confirm
                </label>
              </Button>
            </div>
            <br />
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

export default ChangePasswordForm;
