import * as React from 'react';
import { useSelector } from 'react-redux';
import { Grid, Image, Button } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import styles from 'src/components/thread/components/shared-post-link/styles.module.scss';

import { Form } from 'semantic-ui-react';
import UpdateProfile from './update-profile';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [editingProfile, setEditingProfile] = React.useState(false);

  return (
    <Grid
      container
      textAlign="center"
      style={{ paddingTop: 30 }}
    >
      <Grid.Column>
        <Image
          centered
          src={user.image?.link ?? DEFAULT_USER_AVATAR}
          size="medium"
          circular
        />
        <br />
        <div style={{ maxWidth: '25%', margin: 'auto' }}>
          <Form>
            <Form.Input
              icon="user"
              iconPosition="left"
              placeholder="Username"
              type="text"
              disabled
              value={user.username}
            />
            <Form.TextArea
              disabled
              placeholder="Status"
              rows={6}
              value={user.status}
            />
            <Form.Input
              icon="at"
              iconPosition="left"
              placeholder="Email"
              type="email"
              disabled
              value={user.email}
            />
          </Form>
        </div>
        <br />
        <br />
        <Button
          color="teal"
          iconName={IconName.EDIT}
          onClick={() => setEditingProfile(true)}
        >
          <label className={styles.btnImgLabel}>
            Edit
          </label>
        </Button>
        {editingProfile && (
          <UpdateProfile
            user={user}
            close={() => setEditingProfile(false)}
          />
        )}
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
